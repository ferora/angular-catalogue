import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { Catalogue } from './model/catalogue';
import { CatalogueService } from './service/catalogue.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    catalogue:Catalogue;
    catalogueSubscription:Subscription; // ! Ne peut pas être dans le constructeur !

    constructor(
        private catalogueService:CatalogueService
    ) {
    }

    /*
     * Initiation asynchrone du catalogue au lancement du component
     */
    ngOnInit() {
      this.catalogueSubscription = this.catalogueService.catalogueSubject.subscribe(
        (e:Catalogue) => {
          this.catalogue = e;
        }
      );

      this.catalogueService.emitCatalogueSubject();
    }

}
