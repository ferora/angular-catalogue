import { BrowserModule } from '@angular/platform-browser';

//Formulaires
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

//Gestion du HTTP
import { HttpClientModule } from '@angular/common/http';

//Gestion des « routes » qui permettent de « paginer » l’appplication
import { Routes, RouterModule } from '@angular/router';

//Components
import { AppComponent } from './app.component';
import { AddComponent } from './add/add.component';
import { HomeComponent } from './home/home.component';
import { LostComponent } from './lost/lost.component';

//Services
import { CatalogueService } from './service/catalogue.service';

//Constantes
const appRoutes = [
  //Les différentes pages
  { path: 'home', component: HomeComponent },
  { path: 'add', component: AddComponent },
  //Page d’accueil
  { path: '', component: HomeComponent },
  //Toujours à la fin => toutes directions
  { path: '**', component: LostComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    AddComponent,
    HomeComponent,
    LostComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule,
    BrowserModule
  ],
  providers: [
    CatalogueService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
