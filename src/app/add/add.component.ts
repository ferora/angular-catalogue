import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';

import { Movie } from '../model/movie';
import { Catalogue } from '../model/catalogue';
import { CatalogueService } from '../service/catalogue.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
    catalogue:Catalogue;
    catalogueSubscription:Subscription; // ! Ne peut pas être dans le constructeur !

    movieSearched:Movie[] = [];

    constructor(
        private catalogueService:CatalogueService
    ) {
    }

    /*
     * Initiation asynchrone du catalogue au lancement du component
     */
    ngOnInit() {
      this.catalogueSubscription = this.catalogueService.catalogueSubject.subscribe(
        (e:Catalogue) => {
          this.catalogue = e;
        }
      );

      this.catalogueService.emitCatalogueSubject();
    }

    /*
     * Recherche de films à partir du formulaire
     * dont les résultats sont assignés localement
     * puis remise à zéro du formulaire.
     * @param form:NgForm objet de formulaire angular
     */
    searchMovie(form:NgForm) {
        this.movieSearched = this.catalogueService.searchByName(form.value["nameMovie"]);
        form.reset();
    }

    /*
     * Ajout d’un film à partir de l’index du tableau de recherche
     * @param imdbID:string
     */
    addMovie(index:number) {
        let flag = this.catalogueService.add(this.movieSearched[index].imdbID);
        console.log(flag);

        //TODO ne fonctionne pas à cause de l’asynchronisme
        /*
        if ( flag === true ) {
            this.movieSearched.splice(index, 1);
        }
        else {
            alert('L’ajout du film au catalogue a échoué. Désolé.');
        }*/
    }
}
