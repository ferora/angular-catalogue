export class Movie {
    constructor (
        public imdbID:string,
        public name:string,
        public year:number,
        public pathPoster:string,
        public runtime:string,
        public genre:string,
        public type:string,
        public synopsis:string
    ) {
    }
}
