//Pour faire des requêtes HTTP avec des objets
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

// Models
import { Catalogue } from '../model/catalogue';
import { Movie } from '../model/movie';

//Constantes
const ROOTPATH:string = "http://www.omdbapi.com/?apikey=a42fa07f&type=movie";
const PATHBDD:string = "https://danstonq-31fc8.firebaseio.com/catalogue-flo.json";

@Injectable()
export class CatalogueService {
    //Le catalogue du service ne doit pas être directement accessible
    //pour être sûr qu’il soit synchrone
    private catalogue:Catalogue = new Catalogue();
    //Objet de mise à jour asynchrone
    catalogueSubject = new Subject();

    constructor(
        private httpClient:HttpClient
    ) {
        this.get();
    }

    /* *********************************
       * Gestion de la base de données *
       ********************************* */

    /*
     * Mise à jour de l’objet catalogue
     */
    emitCatalogueSubject() {
        this.catalogueSubject.next(this.catalogue);
    }

    /*
     * Mise à jour de la base de données à partir du catalogue
     */
    put() {
      this.httpClient
        .put(PATHBDD, this.catalogue)
        .subscribe(
          () => {
            console.log('Tout s’est bien passé. Ouf !');
          },
          (error) => {
            console.log('Aïe ! Il y a eu un soucis… Fais plus attention !');
          }
        );
    }

    /*
     * Récupération des données depuis la base de données
     */
    get() {
      this.httpClient
        .get<Catalogue>(PATHBDD)
        .subscribe(
          (response) => {
            this.catalogue = response;

            if ( this.catalogue === undefined || this.catalogue === null ) {
                this.catalogue = new Catalogue();
            }

            //Mise à jour asynchrone du catalogue
            this.emitCatalogueSubject();

            console.log('Tout va bien. La récupération a fonctionné.');
          },
          (error) => {
            console.log('Tout va mal. Tout est perdu !!');
          }
        );
    }

    /* *********************
       * Gestion des films *
       ********************* */

    /*
     * Fait une recherche sur omdB à partir d’une chaîne de caractères
     * @param search:string chaîne à rechercher
     * @return Movies[] tableaux des résultats de la recherche, peut être vide
     */
     //TODO Afficher plus d’une page de résultat
     //TODO Fournir la possibilité de choisir le type à rechercher : movie, serie, episode
    searchByName(search:string) {
        //Pour accèder à l’instance dans le « then() »
        let that = this;

        let films:Movie[] = [];

        if ( search != undefined && search != null && search != '' ) {
          this.getData(this.getPath(true, search))
              .then(function (response) {
                  if ( response.Response === "True" ) {
                      for ( let r of response.Search ) {
                          if ( that.searchByImdbIDInCatalogue(r["imdbID"]) === -1 ) {
                              films.push( new Movie(
                                    r["imdbID"],
                                    r["Title"],
                                    parseInt(r["Year"]),
                                    r["Poster"],
                                    r["Runtime"],
                                    r["Genre"],
                                    r["Type"],
                                    r["Plot"]
                                  )
                              );
                          }
                      }

                      console.log('Response : ' + Object.keys(response));
                      console.log('Nb films : ' + films.length);
                  }
                  else {
                      console.log('Le service n’a pas pu répondre.');
                  }
              });
        }
        else {
            console.log('La recherche est impossible à partir de ça !');
        }

        return films;
    }

    /*
     * Ajouter un film au catalogue à partir de son imdbID
     * @param imdbID:string
     * @return boolean true ssi l’ajout a bien été fait, false sinon
     */
    add(imdbID:string) {
        let flag:boolean = false;

        if ( this.searchByImdbIDInCatalogue(imdbID) === -1 ) {
            //Pour utilise « this » dans le « then() »
            let that = this;

            flag = this.getData(this.getPath(false, imdbID)).then(function (response) {
                  console.log(response);

                  if ( response != undefined && response != null ) {
                        let film:Movie = new Movie(
                          response["imdbID"],
                          response["Title"],
                          parseInt(response["Year"]),
                          response["Poster"],
                          response["Runtime"],
                          response["Genre"],
                          response["Type"],
                          response["Plot"]
                      )

                    return that.addMovie(film);
                  }
              } );
        }
        else {
            console.log('Le film est déjà dans le catalogue.');
        }

        return flag;
    }

    /*
     * Ajouter un film au catalogue
     * @param film:Movie
     * @return boolean true ssi l’ajout au catalogue a été réalisé, false sinon
     */
    addMovie(film:Movie) {
        let flag:boolean;

        if ( film != undefined && film != null ) {
            this.catalogue.movies.push(film);

            if ( this.catalogue.movies.indexOf(film) > -1 ) {
                console.log('Ajout effectué.');
                flag = true;
                this.put();
            }
            else {
                console.log('L’ajout n’a pas été réalisé.');
            }
        }
        else {
            flag = false;
            console.log('Ce film est invalide. C’est n’importe quoi !!');
        }

        return flag;
    }

    /*
     * Suppression d‘un film du catalogue à partir de son imdbID
     * @param imdbID:string
     * @return boolean true ssi la suppression a été réalisée, false sinon
     */
    remove(imdbID:string) {
        let flag:boolean = false;

        if ( imdbID != undefined && imdbID != null && imdbID != '' ) {
            let index = this.searchByImdbIDInCatalogue(imdbID);

            if ( index > -1 ) {
                this.catalogue.movies.splice(index, 1);

                if ( this.searchByImdbIDInCatalogue(imdbID) === -1 ) {
                    console.log('Suppression effectuée.');
                    flag = true;
                    this.put();
                }
                else {
                    console.log('La suppression à l’index ' + index +' n’a pas été effectuée.');
                }
            }
            else {
                console.log('L’imdbID, ' + imdbID +', fourni ne concerne aucun film du catalogue.')
            }
        }
        else {
            console.log('Aucun imdbID valide.');
        }

        return flag;
    }

    /* *********
       * Utils *
       ********* */

    /*
     * Met une majuscule à la première lettre de tous les mots de la chaîne de caractères
     * @param s:string
     * @return string
     */
    capitalize(s){
        return s.toLowerCase().replace( /\b./g, function(a){ return a.toUpperCase(); } );
    };

    /*
     * Retourne le contenu JSON de la requête à omdB à travers une promesse
     * @param path:string url de la requête
     * @return Promize ssi path fourni, null sinon
     */
    getData(path:string) {
        let data = null;

        if ( path != undefined && path != null && path != '' ) {
            data = this.httpClient.get<any>(path).toPromise();
        }
        else {
            console.log('La requête n’a pu être réalisée.');
        }

        return data;
    }

    /*
     * Retourne l’url de la requête à soumettre à omdB
     * @param name:boolean true ssi c’est requête de recherche par nom, false si c’est par imdbID
     * @param search:string critère de recherche (chaîne de caractères ou imdbID)
     * @return string url de la requête
     */
    getPath(name:boolean, search:string) {
        let path:string = '';

        if ( search!= undefined && search != null && search != '' ) {
            if ( name === true ) {
                path = ROOTPATH +"&s=" + search.replace(' ', '+');
            }
            else if ( name === false ) {
                path = ROOTPATH + "&i=" + search;
            }
            else {
                path = ROOTPATH;
            }
        }
        else {
            path = ROOTPATH;
        }

        console.log(path);

        return path;
    }

    /*
     * Fait une recherche dans le catalogue des films à partir de l’imdbID
     * @param imdbID:string
     * @return number -1 ssi l’imdbID est introuvable, l’indice où il a été trouvé sinon
     */
    searchByImdbIDInCatalogue(imdbID:string) {
        let index:number = -1;

        for ( let i in this.catalogue.movies ) {
            if ( this.catalogue.movies[i].imdbID === imdbID ) {
                index = parseInt(i);
                break;
            }
        }

        return index;
    }
}
