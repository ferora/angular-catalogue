import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { Catalogue } from '../model/catalogue';
import { CatalogueService } from '../service/catalogue.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    catalogue:Catalogue;
    catalogueSubscription:Subscription; // ! Ne peut pas être dans le constructeur !

    constructor(
        private catalogueService:CatalogueService
    ) {
    }

    /*
     * Initiation asynchrone du catalogue au lancement du component
     */
    ngOnInit() {
      this.catalogueSubscription = this.catalogueService.catalogueSubject.subscribe(
        (e:Catalogue) => {
          this.catalogue = e;
        }
      );

      this.catalogueService.emitCatalogueSubject();
    }

    /*
     * Suppression d’un film à partir de son imdbID
     * @param imdbID:string
     */
    removeMovie(imdbID:string) {
        let flag = this.catalogueService.remove(imdbID);
        console.log(flag);
    }
}
